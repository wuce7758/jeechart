/**
 * There are <a href="https://github.com/thinkgem/jeesite">JeeSite</a> code generation
 */
package com.thinkgem.jeesite.modules.stat.service;

import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.BaseService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.stat.entity.Studentscore;
import com.thinkgem.jeesite.modules.stat.dao.StudentscoreDao;

/**
 * 学生成绩Service
 * @author zhangfeng
 * @version 2014-09-18
 */
@Component
@Transactional(readOnly = true)
public class StudentscoreService extends BaseService {

	@Autowired
	private StudentscoreDao studentscoreDao;
	
	public Studentscore get(String id) {
		return studentscoreDao.get(id);
	}
	
	public Page<Studentscore> find(Page<Studentscore> page, @RequestParam Map<String, Object> paramMap) {
		DetachedCriteria dc = studentscoreDao.createDetachedCriteria();
//		if (StringUtils.isNotEmpty(studentscore.getName())){
//			dc.add(Restrictions.like("name", "%"+studentscore.getName()+"%"));
//		}
		dc.addOrder(Order.desc("id"));
		return studentscoreDao.find(page, dc);
	}
	
}
